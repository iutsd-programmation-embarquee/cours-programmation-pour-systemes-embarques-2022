#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	char Nom[50];
	char Prenom[50];
	int Age;
	char Email[50];

	printf("Donnez le nom : ");
	scanf("%s", Nom);
	
	printf("Donnez le prenom : ");
	scanf("%s", Prenom);
	
	printf("Donnez l'age : ");
	scanf("%d", &Age);
	
	printf("Donnez l'adresse Email : ");
	scanf("%s", Email);
	
	printf("======================================\n");
	printf("Nom : %s\n", Nom);
	printf("Prenom : %s\n", Prenom);
	printf("Age : %d ans\n", Age);
	printf("Email : %s\n", Email);
	printf("======================================\n");
	
	return (EXIT_SUCCESS);
}
