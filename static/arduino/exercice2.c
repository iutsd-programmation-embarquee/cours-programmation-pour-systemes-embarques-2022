#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	float A, B, Somme, Difference;
	float Moyenne;
	
	printf("donner le premier nombre : ");
	scanf("%f", &A);

	printf("donner le second nombre : ");
	scanf("%f", &B);
	
	// Calculs
	Somme = A + B;
	Difference = A - B;
	Moyenne = (A + B) / 2;
	
	// Affichage des résultats
	printf("La somme : %0.2f + %0.2f = %0.2f\n", A, B, Somme);
	printf("La différence : %0.2f - %0.2f = %0.2f\n", A, B, Difference);
	printf("La moyenne de %0.2f et %0.2f est %0.2f\n", A, B, Moyenne);
	
	return (EXIT_SUCCESS);
}
