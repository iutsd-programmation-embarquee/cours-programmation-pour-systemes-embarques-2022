#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int A, B, Somme, Difference;
	float Moyenne;

	printf("donner le premier nombre : ");
	scanf("%d", &A);
	
	printf("donner le second nombre : ");
	scanf("%d", &B);
	
	// Calculs
	Somme = A + B;
	Difference = A - B;
	Moyenne = (A + B) / 2;
	
	// Affichage des résultats
	printf("La somme : %d + %d = %d\n", A, B, Somme);
	printf("La différence : %d - %d = %d\n", A, B, Difference);
	printf("La moyenne de %d et %d est %0.2f\n", A, B, Moyenne);
	
	return (EXIT_SUCCESS);
}
 