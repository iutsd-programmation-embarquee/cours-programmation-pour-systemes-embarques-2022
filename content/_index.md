---
title: "Programmation pour systèmes embarqués"
draft: false
date: 2022-09-05
---

Ce cours fait partie du programme de la licence professionnelle [AMIO](amio)

## Séance 1

### Installer [Rapsberry Pi OS](installation/).

+ Copier l'image du système d'exploitation sur une carte SD. 
+ Retrouver l'appareil sur le réseau local ethernet.
+ [Mettre à jour](linux/paquet) le système.
+ Effectuer une [configuration](installation/configuration) de base.

### Adopter des bonnes pratiques pour la sécutité des accès.
+ Utiliser le gestionnaire de mot de passe [KeePass](security/keepass)
+ Se [connecter à distance avec une clé ssh](linux/utilisateurs/ssh).

### Maîtriser les bases d'un système d'exploitation [Linux](linux).

### Faire du Raspberry un [Point d'accès wifi](communication/accesspoint/).

### Installer [NodeRed](nodered)
+ Sécuriser l'accès
+ Créer un service Web
+ Tester le service Web

### Utiliser un environnement de virtualisation [Docker](docker).

## Séance 2

+ Installer [Mongodb](bdd/mongodb)

+ Créer un [service web](nodered/service) avec NodeRed et MongoDb.

## Séance 3

+ Visualiser les données avec [Grafana](bdd/grafana)

+ [Surveiller](monitoring/) votre installation.


### Contrôler l'accès

+ Installer un système de gestion des [utilisateurs](security/fusionauth)

+ Sécurité avec [jetons](security/jwt)




## Programme SARII

[Arduino](arduino)

[Programmation C](pogrammationc)

## Programme 2021/2022

## Prérequis

+ Connaître les moyens de [contrôle d'accès](security/).
+ Connaître les concepts de la [cryptographie](security/cryptography/).
+ Connaître les différentes méthodes d'[accès sécurisés](security/cryptography/accès).
+ Savoir utiliser un gestionnaire de mots de passe : [KeePass](security/keepass/).
+ Savoir utiliser un [gestionnaire de code source](scm).
+ Connaître Les langages du [web](https://integration-documents-web.netlify.app/) : HTML, CSS, Javascript et SVG

Ce cours fait partie du programme de la licence professionnelle [AMIO](amio)

+ Durcir la [sécurité](security/hardened/).

+ [Développer](development/) à distance depuis Windows.

+ Donner un accès [SFTP](sftp) aux fichiers pour une mise à jour à distance.

### Conserver des données

+ Installer [PostgreSQL](bdd/postgresql/)

+ Exercice sur le [cinéma](bdd/cinema/)

### Surveiller des données

## Héberger une application Web

+ Installer le serveur web [nginx](nginx).

+ Installer le serveur web [apache](apache).

+ Sécuriser la communication avec [HTTPS](security/https)

## Applications

### Héberger une application NodeJS

+ Installer [NodeJS](nodejs/)

+ Créer une API [REST](rest/) avec Express

+ [Documentation](rest/documentation) en OpenAPI et Swagger

+ [GraphQL](rest/graphql)

### Héberger une application avec NodeRed et MongoDB

+ Installer [mongodb](bdd/mongodb)

### Héberger une application .Net

+ Installer [.Net 6](dotnet)


Bluetooth ne fonctionne pas sur Raspberry
+ [Bluetooth](communication/bluetooth)

```mermaid
graph LR

  Navigateur -- 443 --> FusionAuth
  Navigateur -- 443 --> Nginx
  Nginx -- 3000 --> NodeJS
  NodeJS --o Express
  Nginx --> Static
  Express -- 5432 --> bdd[(PostgreSQL)]
```

### Communiquer


## Colophon

+ Ce site est réalisé avec Hugo, une technologie [JAMStack](jamstack).