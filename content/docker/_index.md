---
title: "Virtualisation avec Docker"
draft: false
---

### Réseau

Créer un réseau nommé `iutsd-net`. Nommer un réseau permet à plusieurs conteneur de communiquer entre eux.

```shell-session
docker network create --driver bridge --subnet=192.168.0.0/16 --gateway=192.168.0.1 iutsd-net
```

### MariaDB

Télécharger la dernière image de [MariaDB](https://hub.docker.com/_/mariadb/tags)

```shell-session
docker pull mariadb:latest
```

Lancer un conteneur MariaDB.

```shell-session
docker run --name mariadb-iutsd --volume mariadb-iutsd:/var/lib/mysql -p 0.0.0.0:3306:3306 --net iutsd-net --ip 192.168.0.102 --dns 172.16.6.177 -e MARIADB_ROOT_PASSWORD=A1Zrhp8YYljYMObEJccM ^ -d mariadb:latest
```

### Administration MariaDB

Télécharger [Adminer](https://www.adminer.org/) pour MySQL.

Ouvrir une invite de commande Windows dans le dossier où se trouve le fichier `adminer.php`. Démarrer un serveur php avec ce fichier et ouvrir un navigateur 

```shell-session
start php -S localhost:8099 adminer.php
start http://localhost:8099/
``` 
