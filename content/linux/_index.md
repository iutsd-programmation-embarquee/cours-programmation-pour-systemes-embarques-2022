---
title: "Linux"
draft: false
---

## Utiliser le [système de fichiers](fs)
+ Renommer, déplacer, copier et supprimer des fichiers.
+ Supprimer des dossiers.
+ Connaitre l'[organisation](fs/organisation) et l'arborescence des dossiers

## Installer des [paquets logigicels](paquet)

## Éditer des fichier avec [nano](nano)

## Gérer les [utilisateurs](utilisateurs)
+ Attribuer des [droits](droits)

## Gérer les [réseaux](network)

## Gérer les [services](services)



## Commandes utiles

Éteindre (utilisateur `root` ou `sudo`).

```shell-session
$ shutdown -h now
```

ou

```shell-session
$ halt
```

Redémarrer (utilisateur `root` ou `sudo`).

```shell-session
$ shutdown -r now
```

ou

```shell-session
$ reboot
```